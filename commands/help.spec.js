const help = require("./help");

describe("help", () => {
	it("random string", async () => {
		const res = await help("nothelp");
		expect(res).toBeFalsy();
	});
	it("!help", async () => {
		const res = await help("!help");
		expect(res).toBeTruthy();
	});
});
