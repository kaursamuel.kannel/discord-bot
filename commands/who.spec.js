const who = require("./who");

describe("who command", () => {
	it("no name given in msg", async () => {
		const res = await who("notwho");
		expect(res).toBeFalsy();
	});
	it("!who", async () => {
		const res = await who("!who");
		expect(res).toBeTruthy();
	});
});
