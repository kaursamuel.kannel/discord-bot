const who = require("./who");
const help = require("./help");
const elron = require("./elron");

module.exports = [
	who,
	help,
	elron
];
