module.exports = (message) => {
	if(message.startsWith("!elron")){
		var today = new Date();
		var dd = String(today.getDate()).padStart(2, "0");
		var mm = String(today.getMonth() + 1).padStart(2, "0");
		var yyyy = today.getFullYear();
		today = yyyy + "-" + mm + "-" + dd;
		return ("https://elron.pilet.ee/et/results/Tondi/Saue/"+today);
	}
	else{
		return false;
	}
};
