const Discord = require("discord.js");
const client = new Discord.Client();
const commands = require("./commands");
const cmd = require("node-cmd");
const express = require("express");
const http = require("http");


client.once("ready", () => {
	console.log("Ready!");
	var today = new Date();
	var dd = String(today.getDate()).padStart(2, "0");
	var mm = String(today.getMonth() + 1).padStart(2, "0");
	var yyyy = today.getFullYear();
	today = yyyy + "-" + mm + "-" + dd;
	console.log(today);
});
client.on("message", message => {
	if(message.author.bot){
		return;
	}
	Object.values(commands).forEach((command) => {
		const response = command(message.content);
		if (response) {
		  message.channel.send(response);
		}
	});
});

client.login(process.env.BOT_TOKEN);

const app = express();
app.get("/", (request, response) => {
	console.log(`${Date.now()} Ping Received`);
	response.sendStatus(200);
});
app.listen(process.env.PORT);
setInterval(() => {
	http.get(`http://${process.env.PROJECT_DOMAIN}.glitch.me/`);
}, 270000);
setInterval(() => {
	cmd.run("refresh");
}, 3600000);

app.post("/git", (req, res) => {
	if (req.headers["x-gitlab-event"] === "Push Hook") {
		cmd.run("chmod 777 commands/git.sh");
		cmd.get("commands/git.sh", (err, data) => {
			if (data) console.log(data);
			if (err) console.log(err);
		});
		cmd.run("refresh");

		console.log("> [GITLAB] Updated with origin/master");
	}
	return res.sendStatus(200);
});
